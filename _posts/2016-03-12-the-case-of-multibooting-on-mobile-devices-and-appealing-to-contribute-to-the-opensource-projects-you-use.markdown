---
layout: post
title:  "The case of multibooting on mobile devices and appealing to contribute to the opensource projects you use."
date:   2016-03-12 14:00:00 -0500
tags: mobile, android, multiboot, open source, contribute
---
*NOTE:* This is what happens when your brother calls you up asking how to go back to an old ROM on his Xiaomi Redmi Note 4G LTE. The survey takes you to places!

You can multi-boot your phone just as you do with your PCs: MultiRom, Grub4android.org and OpenBump

 1. **MultiRom** being the most preferred and in active development
 2. **grub4android** is the most interesting for me since it is equivalent to what I am blessed to do everyday on my PC
   * At boot time, you can select which ROM to boot to! Amazing, isn't it?
     
     This way you can test those SailfishOS and Ubuntu Touch being ported to OnePlus One which lack critical functionality; yeah, also *OxygenOS* is there to take a look at

 3. There was a project named *Bump* brought up by some developer on xda-threads which (in layman's terms) modifies your custom kernels so that the recent very restricting LG devices could be tricked into accepting them. The developer tried a lot to keep the project closed-source deliberately and now terinated the project! WHY???

	Because, the project made a few others' minds run with the learned awareness of this new possibility and they published an **OpenBump** project (you get the idea). The Openbump is active till this day. This infuriated the developer of original 'Bump' and (s)he ceased the work.

There's a lot to know, lot to learn and try but one thing to be aware of, from this incident. This is to everyone:

* **Act on the projects you use:** Don't keep complaining that some project is dead or unresponsive as most of these devs are undertaking these besides their regular day jobs. Most of them may not have any monetary gains at all. If you feel some project is useful and has to exist, ask the developers how you can contribute to the project.

* **If not code, then translate:** Most of the project websites have a 'How to Contribute' section - read it. If the project is on Github, look for the relevant 'CONTRIBUTORS' file. If you cannot code, offer to 'Translate' the project to any other languages (I mean 'human languages'!) you are comfortable with.

* **Write up on how you use the software:** Most troubling issue with software projects is the lack of proper documentation. These projects, being developed aside these developers' day jobs, may not contain proper documentation. If you feel the project/app is good, offer to do documentation for it. This way it gets spread to more users which increases the project's survival as well as addition of new features by any of those new users.

* **Developers have life and servers have running costs:** You can always choose to do monetary contributions to those developers of your projects too: look for 'Donate', 'Gittip' etc. provisions in the page.

*The special case of Xiaomi devices:* Well, what's going to happen to that Redmi Note 4G? That's a great different interesting thing! Apparently the updating part of Xiaomi devices is simply an 'Updater app'! So, download your ROM (in this case, the old version) and select 'From file' (or the like) in 'Update' section on the device. That's all! The chosen ROM gets installed!

So no rooting, no adb, no fastboot stuff! Never earlier knew that such flashing process would be officially supported by the manufacturers themselves in this manner!

## References:

<https://www.youtube.com/watch?v=QnhZldSpM98>

<https://www.youtube.com/watch?v=QJIZ1D-k6hc>

<http://forum.xda-developers.com/lg-g2/general/bootloader-grub4android-multiboot-t2898512/page4>

<http://forum.xda-developers.com/sprint-lg-g3/orig-development/bump-sign-unlock-boot-images-lg-phones-t2935278/page6>

<http://apps.codefi.re/bump/>

<http://forum.xda-developers.com/showpost.php?p=56987541>

<http://en.miui.com/download-218.html#363>

<http://en.miui.com/forum.php?mod=viewthread&tid=24860>

<https://forums.oneplus.net/threads/sailfish-os-for-oneplus-one.279537/>

<http://forum.xda-developers.com/showpost.php?p=55715829>

<https://forums.oneplus.net/threads/wip-ubuntu-touch-for-oneplus-one.266170/>

<http://standardsandfreedom.net/index.php/2014/07/14/foss-economics/>

<http://standardsandfreedom.net/index.php/2014/10/12/free-software-money/>
