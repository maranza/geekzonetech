---
layout: post
title:  "A virtual training session on contributing to opensource"
date:   2016-03-30 22:00:00 -0500
tags: open source, contribute, training, summer camp
---
My last post took the direction of 'How to Contribute to open source' in the course and this time, I feed you a bit more on those lines.

A great informative guide I collected from one of the amazing summer camps organized by Durgapur Linux User's Group(dgplug) is here,
[How to get started with Open Source](https://sayanchowdhury.dgplug.org/posts/how-to-get-started-with-open-source.html).

These guys at [Durgapur Linux User's Group(dgplug)](https://dgplug.org) have always been amazing to follow. Every year, they organize a "Virtual Summer Camp" to teach enthusiasts round the world about 'How to use & contribute to various open source projects'. All their proceedings happen through IRC with effective moderation. Everyone gets to ask questions and share opinions. You have the opportunity of listening to people from RedHat and such real organisations (Yes, the organisers themselves found their careers at RedHat, HackerEarth, Python Software Foundation etc.)

One can directly talk to the actual developers/community managers at these organisations and learn the process to "get involved" (Hope you learned that term from my last post). I had great time attending their "3-day session on learning Python" in the summer of 2015. Give it a try to interact with more curious beginners round the world like YOU and seasoned contributors while you stay back in your room/dorm (basement, anyone still? :D ) this hot summer. Register for their offering this year now! : <https://dgplug.org/summertraining16.html>

Get a feel to their sessions from this link I bookmarked: 
<https://web.archive.org/web/20140711234831/>
<http://sayanchowdhury.dgplug.org/irclogs/chandankumar-session.log> 
The logs too are relevant to this post today.
