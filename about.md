---
layout: page
title: About
permalink: /about/
---

Geekzone started out as a world-wide Whatsapp group bringing together tech-enthusiasts of different expertises, different ages with multiple viewpoints to discuss on anything and everything related to computing and information technology, for an open conversation. Technological innovations and the non-technical factors that could influence them are openly exchanged thereby rising the knowedge of everyone involved in a collective fashion. Think of a peer-to-peer system where every participant is with the same power to participate.
